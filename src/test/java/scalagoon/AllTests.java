package scalagoon;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SimplePins.class })

/**
 * Contenitore per tutti i test
 * 
 * @author mim
 *
 */
public class AllTests {

}
